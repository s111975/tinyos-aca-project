configuration HplPatmosMemoryIOC
{
  provides interface HplPatmosMemoryIO as MIO;
}
implementation
{
  components HplPatmosMemoryIOP;
  MIO = HplPatmosMemoryIOP;
}
