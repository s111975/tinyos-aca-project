configuration PlatformUartC
{
  provides interface PatmosMemoryUART as PatmosUart;
}
implementation{
  components
    PlatformUartP,
    new HplPatmosMemoryIOP(0xf0080004) as Uart;

  PlatformUartP.MMUart -> Uart;

  PatmosUart = PlatformUartP.PUart;
}
