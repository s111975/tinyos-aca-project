module PlatformUartP
{
  uses interface HplPatmosMemoryIO as MMUart;
  provides interface PatmosMemoryUART as PUart;
}
implementation
{
  command void PUart.send( char val ){
    call MMUart.set(val);
  }
}
