
generic module HplPatmosGeneralIOP(
  unsigned int port_addr,
  uint8_t pin
)
{
  //provides interface HplPatmosGeneralIO as IO;
  provides interface GeneralIO as IO;
}
implementation
{
  #define PORT (*((volatile _IODEV unsigned *)port_addr))

  async command void IO.set() {atomic PORT |= (0x01 << pin);}
  async command void IO.clr() {atomic PORT &= ~(0x01 << pin);}
  async command void IO.toggle() { atomic PORT ^= (0x01 << pin);}
//  async command bool IO.get() {return (call PORT &(0x01 << pin) != 0);}
  async command bool IO.get() {return 1 ;}
  async command void IO.makeInput() { }
  async command bool IO.isInput() { return FALSE; }
  async command void IO.makeOutput() { }
  async command bool IO.isOutput() { return TRUE; }
}
