configuration HplPatmosGeneralIOC
{
  provides interface HplPatmosGeneralIO as LED1;
  provides interface HplPatmosGeneralIO as LED2;
  provides interface HplPatmosGeneralIO as LED3;
  provides interface HplPatmosGeneralIO as LED4;
  provides interface HplPatmosGeneralIO as LED5;
  provides interface HplPatmosGeneralIO as LED6;
  provides interface HplPatmosGeneralIO as LED7;
  provides interface HplPatmosGeneralIO as LED8;
}
implementation
{
  new HplPatmosGeneralIOP(0xf0090000, 0) as L1,
  new HplPatmosGeneralIOP(0xf0090000, 1) as L2,
  new HplPatmosGeneralIOP(0xf0090000, 2) as L3,
  new HplPatmosGeneralIOP(0xf0090000, 3) as L4,
  new HplPatmosGeneralIOP(0xf0090000, 4) as L5,
  new HplPatmosGeneralIOP(0xf0090000, 5) as L6,
  new HplPatmosGeneralIOP(0xf0090000, 6) as L7,
  new HplPatmosGeneralIOP(0xf0090000, 7) as L8

  LED1 = L1;
  LED2 = L2;
  LED3 = L3;
  LED4 = L4;
  LED5 = L5;
  LED6 = L6;
  LED7 = L7;
  LED8 = L8;
}
