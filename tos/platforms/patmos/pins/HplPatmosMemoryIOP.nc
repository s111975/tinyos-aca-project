#include <machine/spm.h>

generic module HplPatmosMemoryIOP(
  unsigned int port_addr
)
{
  provides interface HplPatmosMemoryIO as IO;
  //provides interface GeneralIO as IO;
}
implementation
{
  #define PORT (*((volatile _IODEV unsigned *)port_addr))
  //volatile _SPM int *PORT = (volatile _SPM int *) port_addr;

  command void IO.set(char val) {
    *PORT = val;
  }
  command void IO.clr(char val) {
    *PORT = ~val;
  }
  command void IO.toggle(char val) {
    *PORT ^= val;
  }
}
