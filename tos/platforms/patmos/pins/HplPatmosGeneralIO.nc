interface HplPatmosGeneralIO
{
  async command void set();
  async command void clr();
  async command void toggle();
  async command uint8_t getRaw();
  async command bool get();
  async command void makeInput();
  async command bool isInput();
  async command void makeOutput();
  async command bool isOutput();
}
