#include "hardware.h"
#include <stdio.h>
#include <stdlib.h>

module PlatformP{
  provides interface Init;
  uses interface Init as LedsInit;
}
implementation {
  command error_t Init.init() {
    call LedsInit.init();
    putchar('0');
    return SUCCESS;
  }
}
