module PlatformLedsP
{
  provides interface GeneralIO as L0;
  provides interface GeneralIO as L1;
  provides interface GeneralIO as L2;
  uses interface HplPatmosMemoryIO as MMLed;
  uses interface Init;
}
implementation
{

  async command void L0.set() {
    call MMLed.setPin(1);
  }

  async command void L0.clr() {
    call MMLed.clr(1);
  }

  async command void L0.toggle() {
    call MMLed.toggle(1);
  }

  async command bool L0.get() {
    return FALSE;
  }

  async command void L0.makeInput() {
  }

  async command void L0.makeOutput() {

  }

  async command void L1.set() {
    call MMLed.setPin(2);
  }

  async command void L1.clr() {
    call MMLed.clr(2);
  }

  async command void L1.toggle() {
    call MMLed.toggle(2);
  }

  async command bool L1.get() {
    return FALSE;
  }

  async command void L1.makeInput() {
  }

  async command void L1.makeOutput() {

  }

  async command void L2.set() {
    call MMLed.setPin(4);
  }

  async command void L2.clr() {
    call MMLed.clr(4);
  }

  async command void L2.toggle() {
    call MMLed.toggle(4);
  }

  async command bool L2.get() {
    return FALSE;
  }

  async command void L2.makeInput() {
  }

  async command void L2.makeOutput() {

  }

  async command bool L0.isInput() {
    return FALSE;
  }

  async command bool L0.isOutput() {
    return FALSE;
  }

  async command bool L1.isInput() {
    return FALSE;
  }

  async command bool L1.isOutput() {
    return FALSE;
  }

  async command bool L2.isInput() {
    return FALSE;
  }

  async command bool L2.isOutput() {
    return FALSE;
  }

}
