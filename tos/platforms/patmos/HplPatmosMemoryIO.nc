interface HplPatmosMemoryIO
{
  command void setPin(char val);
  command void set(char val);
  command void clr(char val);
  command void toggle(char val);
}
