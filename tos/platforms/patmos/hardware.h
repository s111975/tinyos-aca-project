#ifndef HARDWARE_H
#define HARDWARE_H

#include "patmoshardware.h"


// enum so components can override power saving,
// as per TEP 112.
// As this is not a real platform, just set it to 0.
#endif
