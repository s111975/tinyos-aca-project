configuration PlatformLedsC
{
  provides interface GeneralIO as Led0;
  provides interface GeneralIO as Led1;
  provides interface GeneralIO as Led2;
  uses interface Init;
}
implementation {
  components
    PlatformLedsP,
    PlatformP,
    new HplPatmosMemoryIOP(0xf0090000) as MML;


    Init = PlatformP.LedsInit;

    PlatformLedsP.MMLed -> MML;

    Led0 = PlatformLedsP.L0;
    Led1 = PlatformLedsP.L1;
    Led2 = PlatformLedsP.L2;

}
