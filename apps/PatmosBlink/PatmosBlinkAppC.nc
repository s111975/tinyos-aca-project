configuration BlinkAppC
{
}
implementation
{
  components MainC, PatmosBlinkC, LedsC, PlatformUartC;


  PatmosBlinkC -> MainC.Boot;
  PatmosBlinkC.PatmosUart -> PlatformUartC;

  PatmosBlinkC.Leds -> LedsC;
}

