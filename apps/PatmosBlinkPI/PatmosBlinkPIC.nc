#include <machine/spm.h>

module PatmosBlinkPIC @safe()
{
  uses interface Leds;
  uses interface Boot;
}
implementation
{

  void toggle1();
  void toggle2();

  void loopWait(){
    int i,j;
    for (i=2000; i!=0; --i)
      for (j=2000; j!=0; --j)
        asm("nop");
  }

  void toggle0(){
    call Leds.led0Off();
    loopWait();
    call Leds.led0On();
    toggle1();
  }

  void toggle1(){
    call Leds.led1Off();
    loopWait();
    call Leds.led1On();
    toggle2();
  }

  void toggle2(){
    call Leds.led2Off();
    loopWait();
    call Leds.led2On();
    toggle0();
  }

  event void Boot.booted()
  {
    toggle0();
  }



}

