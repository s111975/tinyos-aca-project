configuration PatmosBlinkPIAppC
{
}
implementation
{
  components MainC, PatmosBlinkPIC, LedsC;


  PatmosBlinkPIC -> MainC.Boot;

  PatmosBlinkPIC.Leds -> LedsC;
}

